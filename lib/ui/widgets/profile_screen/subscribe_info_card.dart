import 'package:flutter/material.dart';
import 'package:book_rent/data/models/user/has_subscription.dart';

class SubscribeInfoCard extends StatelessWidget{
  final int? counter;
  final HasSubscription subscription;

  const SubscribeInfoCard({this.counter, required this.subscription, Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                  child: Column(
                    children: [
                      Text(
                        'Осталось дней',
                        style: TextStyle(
                            color: Colors.grey[400], fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        "${subscription.leftDays}",
                        style: TextStyle(
                          fontSize: 15.0,
                        ),
                      )
                    ],
                  )),
              Container(
                child: Column(children: [
                  Text(
                    'Подписка до',
                    style: TextStyle(
                        color: Colors.grey[400], fontSize: 14.0),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    subscription.dateTill,
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                  )
                ]),
              ),
              Container(
                  child: Column(
                    children: [
                      Text(
                        'План',
                        style: TextStyle(
                            color: Colors.grey[400], fontSize: 14.0),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        subscription.name,
                        style: TextStyle(
                          fontSize: 15.0,
                        ),
                      )
                    ],
                  )),
            ],
          ),
        ));
  }

}