import 'package:flutter/material.dart';
import 'package:book_rent/data/models/user/has_subscription.dart';

class PlanAdvertisementShort extends StatelessWidget{

  const PlanAdvertisementShort({ Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
        color: Colors.blueAccent,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(

                child: Text(
                  'У вас не активирована подписка', style: Theme.of(context).textTheme.subtitle1!.apply(color: Colors.white),
                ),
              )
            ],
          ),
        ));
  }

}