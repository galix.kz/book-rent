import 'package:flutter/material.dart';

class MenuCard extends StatelessWidget{
  final IconData icon;
  final String title;
  final VoidCallback onPress;

  const MenuCard({Key? key, required this.icon, required this.title, required this.onPress}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child:
          InkWell(
            onTap: onPress,
            child: Container(
              padding: EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
              child: Row(
                  children: [
                    Icon(icon, size: 30,),
                    SizedBox(width: 20,),
                    Expanded(child:  Text(title, style: Theme.of(context).textTheme.subtitle1,)
                    )

                  ]
              ),
            ),
          ),

    );
  }

}