import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:book_rent/ui/widgets/taken_book_card.dart';

import '../../data/models/taken_book/taken_book_model.dart';


class LastTakenBook extends StatelessWidget {
  final TakenBook takenBook;
  const LastTakenBook({required this.takenBook, Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return
      Container(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Вы читаете', style: Theme.of(context).textTheme.headline5!.apply(fontWeightDelta: 3),),
                  ],
                ),
                SizedBox(height: 5,),
                TakenBookCard(takenBook: takenBook),
              ]
          )
      );
  }
}