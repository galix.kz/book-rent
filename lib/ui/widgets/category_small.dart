import 'package:flutter/material.dart';

class CategorySmall extends StatelessWidget{
  final String name;

  const CategorySmall({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DecoratedBox(
      decoration: const BoxDecoration(color: Colors.yellow, borderRadius: BorderRadius.all(Radius.circular(30))),
      child:
      Container(
        padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
        child: Text(this.name, style: TextStyle(color: Colors.black),)
      )
    );
  }

}