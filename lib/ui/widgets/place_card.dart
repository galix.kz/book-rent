import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'loading_widget.dart';

class PlaceCard extends StatelessWidget {
  final String name;
  final String photoUrl;
  final String address;

  const PlaceCard(
      {Key? key,
      required this.name,
      required this.photoUrl,
      required this.address})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListTile(
      title: Text(
        name,
        style: Theme.of(context).textTheme.subtitle1!.apply(fontWeightDelta: 3),
      ),
      subtitle: Text(address),
      leading: SizedBox(
        height: 140,
        width: 70,
        child: CachedNetworkImage(
          imageUrl: '$photoUrl',
          width: 80.0,
          placeholder: (context, url) => LoadingWidget(
            isImage: true,
          ),
          fit: BoxFit.cover,
        ),
      ),
      trailing: ElevatedButton(
          onPressed: () {},
          child: Text('2gis'),
          style: ElevatedButton.styleFrom(
              // Foreground color
              onPrimary: Colors.black,
              // Background color
              primary: Colors.yellow,
              elevation: 0,
          )),
      isThreeLine: true,
    );
  }
}
