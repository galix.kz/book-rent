import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/book/book_store.dart';
import 'package:book_rent/ui/widgets/loading_widget.dart';
import 'package:uuid/uuid.dart';
import 'package:vrouter/vrouter.dart';

import '../../data/models/book/book_model.dart';

class BookCard extends StatelessWidget {
  final String img;
  final Book book;

  BookCard({
    Key? key,
    required this.img,
    required this.book,
  }) : super(key: key);

  static final uuid = Uuid();
  final String imgTag = uuid.v4();
  final String titleTag = uuid.v4();
  final String authorTag = uuid.v4();

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 120.0,
        child:InkWell(
          onTap: (){
            serviceLocator<BookStore>().redirectToBook(book);
            context.vRouter.toNamed(BOOK_ROUTE, pathParameters: {"id": book.id.toString()});
          },
          child: Column(
            children: [
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                elevation: 4.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                    child: Hero(
                      tag: imgTag,
                      child: CachedNetworkImage(
                        imageUrl: '$img',
                        width: 140.0,
                        height: 160,
                        placeholder: (context, url) => LoadingWidget(
                          isImage: true,
                        ),
                        fit: BoxFit.cover,
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                  ),
                ),
              Text(book.name, overflow: TextOverflow.ellipsis,),
            ],
          ),
        ) );
  }
}
