import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../data/models/taken_book/taken_book_model.dart';

class TakenBookCard extends StatelessWidget{
  final TakenBook takenBook;
  const TakenBookCard({required this.takenBook, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
        child: Container(
            padding: EdgeInsets.all(15),
            alignment: Alignment.topLeft,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 140,
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                      child: CachedNetworkImage(
                          imageUrl: takenBook.bookCopy.book.cover)),
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(takenBook.bookCopy.book.name,
                        style: Theme.of(context).textTheme.bodyText1!.apply(fontWeightDelta: 2, fontSizeDelta: 3),),
                      SizedBox(height: 5,),
                      Text(takenBook.bookCopy.book.authors ?? "",
                        style: Theme.of(context).textTheme.bodyText1,),
                      SizedBox(height: 10,),
                      Text('День ${takenBook.leftDays} из 21', style: Theme.of(context).textTheme.bodyText2,),
                      SizedBox(height: 10,),
                      SizedBox(
                        width: MediaQuery.of(context).size.width*0.3,
                        height: 5,
                        child: LinearProgressIndicator(
                          semanticsLabel: 'Linear progress indicator',
                          value: takenBook.leftDays/21,
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                )

              ],
            )
        )

    );
  }

}