import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:book_rent/router/const.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:vrouter/vrouter.dart';

import '../../data/models/book/book_model.dart';
import '../../service_locator.dart';
import '../../stores/book/book_store.dart';
import 'loading_widget.dart';

class BookCardWithDescription extends StatelessWidget {
  final Book book;

  const BookCardWithDescription({Key? key, required this.book})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(

      child: Card(
       child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: InkWell(
            onTap: (){
              serviceLocator<BookStore>().redirectToBook(book);
            context.vRouter.toNamed(BOOK_ROUTE, pathParameters: {"id": book.id.toString()});},
              child: (Row(
                children: [
                  CachedNetworkImage(
                    imageUrl: book.cover,
                    width: 100.0,
                    placeholder: (context, url) => LoadingWidget(
                      isImage: true,
                    ),
                    fit: BoxFit.cover,
                  ),
                  Container(
                    padding:
                        EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          book.name,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width*0.5,
                          child: Text(
                            book.description,
                            style: Theme.of(context).textTheme.subtitle1,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        RatingBar.builder(
                          initialRating: book.rating,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemSize: 25,
                          itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                          itemBuilder: (context, _) => const Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ))),
        )

      ),
    );
  }
}
