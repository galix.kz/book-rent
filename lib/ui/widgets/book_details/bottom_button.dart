import 'package:flutter/material.dart';

class BottomButton extends StatelessWidget{
  final VoidCallback onPressed;
  const BottomButton({required this.onPressed, Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return ElevatedButton(onPressed: onPressed, child: Text('Арендовать'),

      style: ElevatedButton.styleFrom(
        primary: Colors.purple,
        padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
        textStyle:
        const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),);

  }

}