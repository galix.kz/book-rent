import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../../data/models/book/book_detailed_model.dart';
import '../category_small.dart';
import '../loading_widget.dart';

class DetailHeader extends StatelessWidget {
  BookDetailed? book;

  DetailHeader({required this.book, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: CachedNetworkImage(
            imageUrl: book!.cover,
            width: 150.0,
            placeholder: (context, url) => LoadingWidget(
              isImage: true,
            ),
            fit: BoxFit.cover,
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 20, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width*0.4,
                  child: Text(
                    book!.name,
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .apply(fontWeightDelta: 2),
                  )),
              SizedBox(
                height: 10,
              ),
              Text(
                book!.authors!,
                style: Theme.of(context).textTheme.subtitle1,
              ),
              SizedBox(
                height: 10,
              ),
              RatingBar.builder(
                initialRating: book!.rating,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemSize: 25,
                itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  print(rating);
                },
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                  width: 160,
                  child: Wrap(
                      direction: Axis.horizontal,
                      spacing: 10,
                      runSpacing: 5,
                      children: (book!.categories != null)
                          ? book!.categories!
                              .map(
                                (e) => CategorySmall(name: e.name),
                              )
                              .toList()
                          : []))
            ],
          ),
        )
      ],
    );
  }
}
