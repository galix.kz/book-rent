import 'package:flutter/material.dart';
import 'package:book_rent/data/models/subscription/subscription_plan.dart';

import '../../../config/hex_color.dart';

class PlanCard extends StatelessWidget{
  final SubscriptionPlan plan;
  bool selected = false;
  VoidCallback onSelected;
  PlanCard({required this.plan, required this.selected, required this.onSelected, Key? key}) : super(key: key);

  List<Color> nonActiveColors =  [
    Colors.blueAccent,
    Colors.red
  ];
  List<Color> activeColors =  [
    Colors.lightBlueAccent,
    Colors.redAccent
  ];


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      child: DecoratedBox(
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: (selected) ? activeColors : nonActiveColors)),
        child: Container(
          color: HexColor("#190742"),
          margin: EdgeInsets.all(1),
          child: OutlinedButton(
            onPressed: () {
              onSelected();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(plan.name, style: TextStyle(color: selected ? Colors.lightBlue : Colors.indigo),),
                Text('${plan.price} тг', style: TextStyle(color: selected ? Colors.lightBlue : Colors.indigo),)
              ],
            ),
          ),
        ),
      ),
    );
  }

}