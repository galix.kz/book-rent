import 'package:flutter/material.dart';
import 'package:book_rent/router/const.dart';
import 'package:vrouter/vrouter.dart';

class PlanAdvertisementCard extends StatefulWidget {
  const PlanAdvertisementCard({Key? key}) : super(key: key);

  @override
  _PlanCard createState() => _PlanCard();
}

class _PlanCard extends State<PlanAdvertisementCard> {
  bool planValue = false;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
          onTap: (){
            context.vRouter.toNamed(SUBSCRIPTION_LIST_SCREEN_ROUTE);
          },
      child: Container(
          color: Colors.blueAccent,
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          child: Column(
            children: [
              Text(
                'У вас не активирована подписка',
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .apply(color: Colors.white),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                  'Купите подписку для доступа к более соверщенному функционалу',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .apply(color: Colors.white),
                  textAlign: TextAlign.center),
              SizedBox(
                height: 10,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  'Всего за',
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1!
                      .apply(color: Colors.white),
                ),
                Text(
                  ' 990 тг/месяц',
                  style: Theme.of(context).textTheme.subtitle1!.apply(
                      color: Colors.white,
                      fontSizeDelta: 1,
                      fontWeightDelta: 6),
                )
              ])
            ],
          )),
    ));
  }
}
