import 'package:flutter/material.dart';

class HomeHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/img/Header.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: SizedBox(
            height: 200,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Привет, Читатель 👋',
                    style: Theme.of(context)
                        .textTheme
                        .headline5!
                        .apply(color: Colors.white, fontWeightDelta: 3),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Что вы хотите прочитать сегодня?',
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .apply(color: Colors.white70, fontWeightDelta: 0),
                  )
                ],
              ),
            ),
          ) /* add child content here */,
        ),
        Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.only(top: 160, right: 20.0, left: 20.0),
          child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ListTile(
                    title: Container(
                        padding: EdgeInsets.only(bottom: 5, top: 10),
                        child: Text(
                          'Цитата дня',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.w700),
                        )),
                    subtitle: Container(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          'Читая каждый день вы развиваете не только себя ',
                          textAlign: TextAlign.center,
                        )),
                    tileColor: Color.fromRGBO(255, 227, 76, 1),
                  ),
                ],
              )),
        )
      ],
    );
  }
}
