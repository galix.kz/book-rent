import 'package:flutter/material.dart';

import '../../data/models/book/book_model.dart';
import 'book_card.dart';
import 'loading_widget.dart';

class BookCarousel extends StatelessWidget {
  final String title;
  final String showAllUrl;
  final List<Book> books;
  final TextStyle? titleStyle;

  const BookCarousel(
      {Key? key,
      required this.title,
      required this.showAllUrl,
      required this.books,
      this.titleStyle})
      : super(key: key);

  Widget _content() {
    if (books.length == 0) {
      return LoadingWidget(
        isImage: false,
      );
    } else {
      return SizedBox(
        height: 200,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: books.length,
          itemBuilder: (context, index) =>
              BookCard(img: books[index].cover, book: books[index]),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title,
                style: titleStyle ??
                    Theme.of(context)
                        .textTheme
                        .headline5!
                        .apply(fontWeightDelta: 3)),
            // Text('Все', style: Theme.of(context).textTheme.subtitle1)
          ],
        ),
        SizedBox(
          height: 10,
        ),
        _content()
      ],
    );
  }
}
