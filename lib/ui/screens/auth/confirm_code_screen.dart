
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/user/user_store.dart';
import 'package:hive/hive.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:vrouter/vrouter.dart';

import '../../../config/colors.dart';
import '../../../services/notifier_service.dart';
import '../../../stores/auth/auth_store.dart';
import '../../widgets/buttons/custom_button.dart';

class ConfirmPhoneScreen extends StatefulWidget {

  const ConfirmPhoneScreen({Key? key}) : super(key: key);

  @override
  State<ConfirmPhoneScreen> createState() => _ConfirmPhoneScreenState();
}

class _ConfirmPhoneScreenState extends State<ConfirmPhoneScreen> {
  final AuthStore authStore = serviceLocator<AuthStore>();

  String code = '';

  bool hasError = false;

  bool loading = false;

  late TextEditingController controller = TextEditingController();

  Box tokenBox = Hive.box<String>('token');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  SizedBox(height: 157),
                  Text(
                    'Подтвердите номер телефона',
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w700,
                        fontSize: 24),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    'На номер ${authStore.userLogIn!.phone} был отправлен\n SMS-код для подтверждения',
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: AppColors.blackText.withOpacity(0.6)),
                  ),
                  Container(
                    height: 15,
                    child: Observer(builder: (_) {
                      if (authStore.userLogIn!.code == null)
                        return SizedBox(
                          height: 15,
                        );
                      return Text("Код: ${authStore.userLogIn!.code!}");
                    }),
                  ),
                  SizedBox(height: 31),
                  PinCodeTextField(
                    appContext: context,
                    autoFocus: true,
                    controller: controller,
                    obscureText: false,
                    pinTheme: PinTheme(
                      selectedFillColor: Colors.white,
                      inactiveFillColor: Colors.white,
                      activeFillColor: Colors.white,
                      inactiveColor: hasError ? AppColors.danger : Colors.transparent,
                      activeColor:
                          hasError ? AppColors.danger : Colors.transparent,
                      selectedColor: hasError ? AppColors.danger : Colors.white,
                      shape: PinCodeFieldShape.box,
                      borderRadius: BorderRadius.circular(5),
                      borderWidth: 2,
                      fieldHeight: 64,
                      fieldWidth: 45,
                    ),
                    enableActiveFill: true,
                    backgroundColor: Colors.transparent,
                    length: 6,
                    obscuringCharacter: ' ',
                    onChanged: (text) {
                      setState(() {
                        hasError = false;
                      });
                    },
                    onCompleted: handleDone,
                    textStyle:
                        TextStyle(fontSize: 22.0, color: AppColors.primaryText),
                    animationType: AnimationType.scale,
                    animationCurve: Curves.easeInOut,
                    animationDuration: Duration(milliseconds: 100),
                    keyboardType: TextInputType.number,
                  ),
                ],
              ),
              Column(
                children: [
                  CustomButton(
                    text: (hasError) ? "Ошибка!" : (loading) ? "Загрузка" : "Далее",
                    isActive: (!hasError && controller.text.length == 6),
                    press: () {

                      if(controller.text.length == 6 )
                        handleSubmit();
                      else{
                        serviceLocator<NotifierService>().showErrorSnackbar<dynamic>(context: context,
                            message: "Введите правильный код!");
                      }
                    },
                  ),
                  SizedBox(height: 49)
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void handleDone(String _code) async {
    AuthStore authStore = serviceLocator<AuthStore>();
    setState(() {
      hasError = false;
      loading = true;
      code = _code;
    });

    handleSubmit();
  }

  void handleSubmit() async {
    try {
      if (await authStore.checkVerifyCode(code)) {
        await authStore.logIn();
        print(serviceLocator<UserStore>().user);
        setState(() {
          loading = false;
        });
        if (serviceLocator<UserStore>().user?.isNew == false) {
          if(tokenBox.values.isNotEmpty) {

          }
          context.vRouter.toNamed(HOME_ROUTE);
        } else {
          context.vRouter.toNamed(HOME_ROUTE);
        }
      } else {
        setState(() {
          loading = false;
          hasError = true;
        });
        serviceLocator<NotifierService>().showErrorSnackbar<dynamic>(context: context,
            message: "Введите правильный код!");
      }
    } catch (e) {
      print(e);
      setState(() {
        loading = false;
        hasError = true;
      });
      serviceLocator<NotifierService>().showErrorSnackbar<dynamic>(context: context,
          message: "Системная ошибка! Повторите позднее");
      // throw e;
    }
  }

}
