import 'package:flutter/material.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/service_locator.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:vrouter/vrouter.dart';

import '../../../config/colors.dart';
import '../../../services/notifier_service.dart';
import '../../../stores/auth/auth_store.dart';
import '../../widgets/buttons/custom_button.dart';
import '../../widgets/form/CustomTextField.dart';


class Auth1Screen extends StatefulWidget {
  @override
  State<Auth1Screen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<Auth1Screen> {
  String phone = '';
  String email = '';
  bool loading = false;

  MultiValidator validator = MultiValidator([
    RequiredValidator(errorText: 'Необходимо ввести номер телефона'),
    PatternValidator(r'(\+7\d{3}\d{3}\d{2}\d{2})',
        errorText: 'Введите корректный номер телефона')
  ]);
  final phoneFormatter = MaskTextInputFormatter(
    mask: '+7##########',
    filter: {"#": RegExp(r'[0-9]')},
  );
  int someval = 0;

  @override
  void initState() {
    super.initState();
    register();
  }

  Future<void> register() async {
    await serviceLocator.allReady();
  }

  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Flexible(
                flex: 3,
                child: Column(
                  children: [
                    SizedBox(height: 197),
                    Text(
                      'Вход в Book Rent',
                      style: TextStyle(
                          fontSize: 32,
                          color: AppColors.blackText,
                          fontWeight: FontWeight.w700),
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Для авторизации или регистрации введите \n Ваш номер телефона',
                      maxLines: 2,
                      style: TextStyle(
                          fontSize: 15,
                          color: AppColors.blackText.withOpacity(0.6),
                          fontWeight: FontWeight.w400),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 32),
                    CustomTextField(
                      controller: controller,
                      labelText: "Телефон",
                      hintText: "+77 - - - - - - - - -",
                      onChange: (value) {
                        setState(() {
                          someval = controller.text.length;
                        });
                        if (controller.text.length == 12)
                        setState(() {
                          phone = value;
                          FocusScope.of(context).requestFocus(new FocusNode());
                        });
                      },
                      formatter: [phoneFormatter],
                        textInputType: TextInputType.number,
                      validator: validator
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Column(
                  children: [
                    TextButton(
                        onPressed: () {
                          // Navigator.pushNamed(context, UserAgreementRoute);
                        },
                        child: Text(
                          "Пользовательское соглашение",
                          style: TextStyle(
                              color: AppColors.blackText.withOpacity(0.6),
                              fontWeight: FontWeight.w700,
                              fontSize: 15),
                        )),
                    SizedBox(height: 20),
                    CustomButton(
                        text: loading ? "Загрузка" : "Далее",
                        isActive: someval == 12,
                        press: () {
                          if(someval == 12) {
                            navigateToCode();
                          } else{
                            serviceLocator<NotifierService>().showErrorSnackbar<dynamic>(context: context,
                            message: "Введите правильный номер");
                          }
                        }),
                    // SizedBox(height: 49)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> navigateToCode() async {
    if (phone.isEmpty) {
      serviceLocator<NotifierService>().showError<dynamic>(error: Exception('Заполните номер'));
    } else {
      AuthStore authStore = serviceLocator<AuthStore>();
      setState(() {
        loading = true;
      });
      await authStore.authByPhone(phone);
      context.vRouter.toNamed(CONFIRM_CODE);
    }
  }
}
