import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/taken_books/taken_books_store.dart';
import 'package:book_rent/ui/widgets/last_taken_book.dart';
import 'package:vrouter/vrouter.dart';

import '../../data/models/taken_book/taken_book_model.dart';
import '../widgets/taken_book_card.dart';

class UserTakenBooks extends StatelessWidget {
  late List<TakenBook> takenBooks;

  @override
  Widget build(BuildContext context) {
    takenBooks = serviceLocator<TakenBooksStore>().takenBooks;
    return Observer(builder: (_) {
      return Scaffold(appBar: AppBar(
          automaticallyImplyLeading: false,
          leadingWidth: 60,
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: InkWell(
            child: IconButton(
              onPressed: () =>
              {context.vRouter.historyBack()},
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              color: Colors.black,
            ),
          ),
      ),
      body:
      Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
          child: ListView.separated(
              scrollDirection: Axis.vertical,
              itemCount: takenBooks.isEmpty ? 1 : takenBooks.length + 1,
              separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
              itemBuilder: (context, index) {
                if (index == 0)
                  return Text(
                    'Мои книги',
                    style: Theme.of(context)
                        .textTheme
                        .headline4!
                        .apply(fontWeightDelta: 3),
                  );
                index -= 1;
                return TakenBookCard(takenBook: takenBooks[index],);
              })
      ));
    });
  }
}
