import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_paybox/paybox.dart';

import 'package:flutter_paybox/environments.dart';
import 'package:book_rent/data/models/subscription/subscription_paybox_create.dart';
import 'package:book_rent/data/models/subscription/subscription_user_history_create.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/subscription/subscription_store.dart';
import 'package:book_rent/stores/user/user_store.dart';
import 'package:uuid/uuid.dart';
import 'package:vrouter/vrouter.dart';


import '../../data/models/user/user.dart';


class PaymentScreen extends StatefulWidget {

  PaymentScreen({ Key? key}) : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  var uuid = Uuid();
  late User? user;
  late SubscriptionPayboxCreate subscriptionPayboxCreate;
  var paybox = Paybox(
    merchantId: 515533,
    secretKey: "WSQB7H64BLpBORzX",
  );

  int? paymentId;

  @override
  void initState() {
    paybox.configuration.testMode = true;
    paybox.configuration.paymentSystem = PaymentSystem.EPAYWEBKGS;
    paybox.configuration.currencyCode = 'KZT';
    // paybox.configuration.autoClearing = false;
    paybox.configuration.encoding = 'UTF-8';
    // paybox.configuration.recurringLifetime = 12;
    // paybox.configuration.paymentLifetime = 12;
    // paybox.configuration.recurringMode = true;
    // paybox.configuration.userEmail = 'user@email.mail';
    // paybox.configuration.language = Language.ru;
    //
    // paybox.configuration.checkUrl = String;
    // paybox.configuration.resultUrl = String;
    // paybox.configuration.refundUrl = String;
    // paybox.configuration.autoClearing = bool;
    // paybox.configuration.requestMethod = RequestMethod;
    user = serviceLocator<UserStore>().user;
    subscriptionPayboxCreate = serviceLocator<SubscriptionStore>().payboxCreateData!;
    if(user!=null) paybox.configuration.userPhone = "+7${user!.username}";
    onCreatePayment();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_){
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: InkWell(
            child: IconButton(
              onPressed: () =>
              {context.vRouter.to(context.vRouter.previousUrl!)},
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              color: Colors.black,
            ),
          ),
          actions: [

          ],
        ),
        body: PaymentWidget(
          controller: paybox.controller,
          onPaymentDone: (success) async {
            print(subscriptionPayboxCreate.orderId);

            print(subscriptionPayboxCreate.paymentId);
            if(subscriptionPayboxCreate.orderId!=null  && subscriptionPayboxCreate.paymentId!=null){
              SubscriptionUserHistoryCreate subscriptionCreateData = SubscriptionUserHistoryCreate(user: user!.id, plan: subscriptionPayboxCreate.plan.id, price: subscriptionPayboxCreate.plan.price, orderId: subscriptionPayboxCreate.orderId!, paymentId: subscriptionPayboxCreate.paymentId!);
              await serviceLocator<SubscriptionStore>().createSubscriptionUserHistory(subscriptionCreateData);
              await serviceLocator<UserStore>().getMe();
              context.vRouter.toNamed(HOME_ROUTE);
            }

          },
        ),
      );
    });

  }

  void onCreatePayment() {
    String orderId = uuid.v4();

    paybox
        .createPayment(
      amount: subscriptionPayboxCreate.plan.price.toDouble(),
      userId: user!.id.toString(),
      orderId: orderId,
      description: "Оплата за план подписки ${subscriptionPayboxCreate.plan.name}",
    )
        .then((payment) async {
      if (payment != null) {
        SubscriptionPayboxCreate oldData = subscriptionPayboxCreate;
        oldData.paymentId = payment.paymentId.toString();
        oldData.orderId = orderId;
        serviceLocator<SubscriptionStore>().setPayboxCreateData(oldData);
      }
    }).onError((error, stackTrace) {
      // Handle PayboxError
    });
  }
}