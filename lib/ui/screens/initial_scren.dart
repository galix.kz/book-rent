import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:book_rent/config/api_routes.dart';
import 'package:book_rent/network/user_client.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/user/user_store.dart';
import 'package:dio/dio.dart';
import 'package:vrouter/vrouter.dart';

import '../../network/interceptors/interceptor.dart';
import '../../network/interceptors/log_interceptor.dart';
import '../../router/const.dart';

class InitialScreen extends StatefulWidget {
  @override
  _InitialScreen createState() => _InitialScreen();
}


class _InitialScreen extends State<InitialScreen> {



  @override
  Widget build(BuildContext context) {
    // TODO remove this page and make normal splashScreen;

    return Container();
  }
}