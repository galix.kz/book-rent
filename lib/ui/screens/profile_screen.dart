import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/user/user_store.dart';
import 'package:book_rent/ui/widgets/home_screen/plan_advertisement_card.dart';
import 'package:book_rent/ui/widgets/profile_screen/menu_card.dart';
import 'package:vrouter/vrouter.dart';

import '../../data/models/user/user.dart';
import '../widgets/profile_screen/plan_advertisement_short.dart';
import '../widgets/profile_screen/subscribe_info_card.dart';

class ProfileScreen extends StatefulWidget {

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfileScreen> {
  int counter = 0;
  User? user;

  Widget _subscription(){
    print(user!.hasSubscription);
    if(user!.hasSubscription==null) return PlanAdvertisementShort();
    return SubscribeInfoCard(counter: counter, subscription: user!.hasSubscription!);
  }
  @override
  Widget build(BuildContext context) {
    user = serviceLocator<UserStore>().user;
    return Observer(builder: (_) {
      return Scaffold(
        body: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  flex: 4,
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [Colors.deepPurple, Colors.deepPurpleAccent],
                      ),
                    ),
                    child: Column(children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.15
                      ),
                      // CircleAvatar(
                      //   radius: 65.0,
                      //   backgroundImage: AssetImage('assets/erza.jpg'),
                      //   backgroundColor: Colors.white,
                      // ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text('+7${user!.username!}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 40.0,
                          )),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        user!.fullname == null ? '' : user!.fullname!,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                        ),
                      )
                    ]),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Container(
                    color: Colors.grey[200],
                    child: Center(
                        child: Container(
                            margin: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
                            child: Container(
                              width: 310.0,
                              height: 290.0,
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    MenuCard(
                                        icon: Icons.book,
                                        title: 'Мои книги',
                                        onPress: (){
                                          context.vRouter.toNamed(TAKEN_BOOKS_ROUTE);
                                        },),
                                    SizedBox(height: 10,),
                                    MenuCard(
                                        icon: Icons.info,
                                        title: 'О приложении',
                                      onPress: (){},),
                                    SizedBox(height: 10,),
                                    MenuCard(
                                        icon: Icons.logout,
                                        title: 'Выйти',
                                      onPress: (){
                                          serviceLocator<UserStore>().logout();
                                          context.vRouter.toNamed(AUTH);
                                      },),
                                  ],
                                ),
                              ),
                            ))),
                  ),
                ),
              ],
            ),

            Positioned(
                top: MediaQuery.of(context).size.height*0.33,
                left: 20.0,
                right: 20.0,
                child: _subscription())
          ],
        ),
      );
    });
  }
}
