import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/data/models/book/book_detailed_model.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/book/book_store.dart';
import 'package:book_rent/stores/user/user_store.dart';
import 'package:book_rent/ui/widgets/book_carousel.dart';
import 'package:book_rent/ui/widgets/book_details/bottom_button.dart';
import 'package:book_rent/ui/widgets/book_details/detail_header.dart';
import 'package:book_rent/ui/widgets/category_small.dart';
import 'package:book_rent/ui/widgets/place_card.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:vrouter/vrouter.dart';

import '../../data/models/book/book_model.dart';
import '../widgets/home_screen/plan_advertisement_card.dart';
import '../widgets/loading_widget.dart';

class BookScreen extends StatelessWidget {
  BookDetailed? book;

  Widget _places() {
    return Observer(builder: (_)
    {
      book = serviceLocator<BookStore>().bookDetail;
      if (serviceLocator<UserStore>().user != null &&
          serviceLocator<UserStore>().user!.hasSubscription != null)
        if (book!.listShopsWithBooks != null && book!.listShopsWithBooks!.isEmpty) return Text('Пока нету в наличии');
        return Column(
          children: (book!.listShopsWithBooks != null)
              ? book!.listShopsWithBooks!
              .map((place) =>
              PlaceCard(
                  name: place.name,
                  photoUrl: place.logo,
                  address: place.address))
              .toList()
              : [],
        );
      return PlanAdvertisementCard();
    });
  }

  Widget _content(BuildContext context) {
    return Observer(builder: (_) {
    book = serviceLocator<BookStore>().bookDetail;
    if (book == null) return LoadingWidget();
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      decoration: BoxDecoration(color: Colors.transparent),
      child: Column(
        children: [
          DetailHeader(book: book),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            width: double.infinity,
            child: Text(
              'Описание',
              style: Theme.of(context).textTheme.headline6,
              textAlign: TextAlign.start,
            ),
          ),

          SizedBox(
            height: 10,
          ),
          ExpandableText(
            book!.description,
            expandText: 'Подробнее',
            collapseText: 'Скрыть',
            maxLines: 10,
            style: Theme.of(context).textTheme.bodyText2!,
            linkColor: Colors.blue,
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            width: double.infinity,
            child: Text(
              'Книга доступна в ',
              style: Theme.of(context).textTheme.headline6,
              textAlign: TextAlign.start,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          _places()
          // BookCarousel(title: 'Рекомендуем', showAllUrl: 'showAllUrl', books: books, titleStyle: Theme.of(context).textTheme.headline6,)
        ],
      ),
    );});
  }

  @override
  Widget build(BuildContext context) {


    return Observer(builder: (_) {
      book = serviceLocator<BookStore>().bookDetail;
      return Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            leadingWidth: 60,
            elevation: 0,
            backgroundColor: Colors.white,
            leading: InkWell(
              child: IconButton(
                onPressed: () =>
                    {context.vRouter.to(context.vRouter.previousUrl!)},
                icon: const Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ),
                color: Colors.black,
              ),
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () async {
                  serviceLocator<BookStore>().setLiked(book!);
                },
                icon: serviceLocator<BookStore>().isLikedContainsDetailedBook
                    ? Icon(
                        Icons.favorite,
                        color: Colors.red,
                      )
                    : Icon(
                        Icons.favorite,
                        color: Colors.black,
                      ),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.share,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          body: Material(
            color: Colors.transparent,
            child: Stack(children: [
              ListView(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 200.0,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                      ),
                      _content(context),
                    ],
                  )
                ],
              ),
              serviceLocator<BookStore>().scannedQr != null
                  ? Positioned(
                      bottom: 20.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: BottomButton(
                          onPressed: () {},
                        ),
                      ))
                  : SizedBox(
                      height: 0,
                    )
            ]),
          ));
    });
  }
}
