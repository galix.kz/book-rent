import 'package:flutter/material.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/router/routes.dart';
import 'package:vrouter/vrouter.dart';

class ScaffoldScreen extends StatefulWidget {
  final Widget child;

  const ScaffoldScreen(this.child);

  @override
  State<StatefulWidget> createState() => _ScaffoldScreen();
}
class _ScaffoldScreen extends State<ScaffoldScreen>{
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,   // <-- HERE
        showUnselectedLabels: false,
        currentIndex: selectedIndex,
        selectedItemColor: Colors.amber[800],
        unselectedItemColor: Colors.black,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home,), label: '', ),
          BottomNavigationBarItem(icon: Icon(Icons.search), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.qr_code), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.bookmark), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: ''),
        ],
        onTap: (int index) {
          setState((){
            selectedIndex = index;
          });
          // We can access this username via the path parameters
          if (index == 0)
            context.vRouter.to(HOME_ROUTE);
          else if(index==1) context.vRouter.to(SEARCH_ROUTE);
          else if(index==2) context.vRouter.toNamed(QR_READ_SCREEN);
          else if(index==3) context.vRouter.to(FAVOURITE_ROUTE);
          else if(index==4) context.vRouter.to(PROFILE_ROUTE);
          else {
            context.vRouter.to('/book');
          }
        },
      ),
      body: Container(
        // padding: EdgeInsets.only(left:20, right: 20),
        child: super.widget.child,),

      // This FAB is shared with login and shows hero animations working with no issues

    ));
  }


}
