import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/config/hex_color.dart';
import 'package:book_rent/data/models/subscription/subscription_paybox_create.dart';
import 'package:book_rent/data/models/subscription/subscription_plan.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/subscription/subscription_store.dart';
import 'package:vrouter/vrouter.dart';
import 'package:flutter_paybox/paybox.dart';

import '../../stores/user/user_store.dart';
import '../widgets/subscription_list_screen/plan_card.dart';

class SubscriptionListScreen extends StatefulWidget {
  const SubscriptionListScreen({Key? key}) : super(key: key);

  @override
  State<SubscriptionListScreen> createState() => _SubscriptionListScreenState();
}

class _SubscriptionListScreenState extends State<SubscriptionListScreen> {

  SubscriptionPlan? selectedPlan;

  var paybox = Paybox(
    merchantId: 515533,
    secretKey: "WSQB7H64BLpBORzX",
  );

  int? paymentId;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Observer(builder: (_) {
      return SafeArea(
          child: Material(
            color: HexColor("#190742"),
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: () {
                            context.vRouter.historyBack();
                          },
                          child: Icon(Icons.close, color: Colors.white60),
                        )
                      ],
                    ),
                    SizedBox(height: 30,),
                    Text('Премиум доступ', style: Theme
                        .of(context)
                        .textTheme
                        .headline4!
                        .apply(color: Colors.white, fontWeightDelta: 3),),
                    SizedBox(height: 20,),
                    Text('Купите подписку и получите целую кучу возможностей',
                      style: Theme
                          .of(context)
                          .textTheme
                          .subtitle1!
                          .apply(color: Colors.white),),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        Icon(Icons.check, color: Colors.white60),
                        SizedBox(width: 10,),
                        Text('Курс по скорочтению', style: Theme
                            .of(context)
                            .textTheme
                            .subtitle1!
                            .apply(color: Colors.white, fontWeightDelta: 2),),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        Icon(Icons.check, color: Colors.white60),
                        SizedBox(width: 10,),
                        Text('Курс по скорочтению', style: Theme
                            .of(context)
                            .textTheme
                            .subtitle1!
                            .apply(color: Colors.white, fontWeightDelta: 2),),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        Icon(Icons.check, color: Colors.white60),
                        SizedBox(width: 10,),
                        Text('Курс по скорочтению', style: Theme
                            .of(context)
                            .textTheme
                            .subtitle1!
                            .apply(color: Colors.white, fontWeightDelta: 2),),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        Icon(Icons.check, color: Colors.white60),
                        SizedBox(width: 10,),
                        Text('Курс по скорочтению', style: Theme
                            .of(context)
                            .textTheme
                            .subtitle1!
                            .apply(color: Colors.white, fontWeightDelta: 2),),
                      ],
                    ),
                    SizedBox(height: 40,),
                    ...serviceLocator<SubscriptionStore>().plans.map((e) =>  Column(
              children: [SizedBox(height: 20,), PlanCard(plan: e, selected: selectedPlan!=null && e.id == selectedPlan!.id, onSelected: (){
                setState(() {
                  selectedPlan = e;
                });
              },)],
            )).toList(),
                    SizedBox(height: 40,),
                    Container(
                      width: double.infinity,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Colors.blueAccent,
                              Colors.red
                            ])),
                        child: Container(
                          color: Colors.transparent,
                          margin: EdgeInsets.all(1),
                          child: OutlinedButton(
                            onPressed: () {
                              SubscriptionPayboxCreate data = SubscriptionPayboxCreate(plan: selectedPlan!);
                              serviceLocator<SubscriptionStore>().setPayboxCreateData(data);
                              context.vRouter.toNamed(PAYMENT_SCREEN_ROUTE);
                            },
                            child: Text('КУПИТЬ', style: Theme
                                .of(context)
                                .textTheme
                                .headline6!
                                .apply(color: Colors.white),),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
            ),
          ));
    },);
  }
}
