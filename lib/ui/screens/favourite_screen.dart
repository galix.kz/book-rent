  import 'package:flutter/material.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/book/book_store.dart';
import 'package:book_rent/ui/widgets/book_card_with_description.dart';

import '../../data/models/book/book_model.dart';

class FavouriteScreen extends StatelessWidget{
  BookStore bookStore = serviceLocator<BookStore>();
  @override
  Widget build(BuildContext context) {
    List<Book?> books =  bookStore.likedBooks;
    return  Material(
      color: Colors.transparent,
      child: Container(
        padding: EdgeInsets.only(left: 15, right: 15, top: 40),
        child: ListView.separated(
          scrollDirection: Axis.vertical,
          itemCount: books == null ? 1 : books.length + 1,
          separatorBuilder: (BuildContext context, int index) => const Divider(),
          itemBuilder: (context, index){
            if(index==0) return Text('Вам нравиться', style: Theme.of(context).textTheme.headline4!.apply(fontWeightDelta: 3),);
                index -= 1;
            return BookCardWithDescription(book: books[index]!);
          }

        )
      ),
    );
  }

}