import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/book/book_store.dart';
import 'package:book_rent/stores/taken_books/taken_books_store.dart';
import 'package:book_rent/ui/widgets/book_card.dart';
import 'package:book_rent/ui/widgets/book_carousel.dart';
import 'package:book_rent/ui/widgets/home_screen/home_header.dart';

import '../../data/models/book/book_model.dart';
import '../../data/models/taken_book/taken_book_model.dart';
import '../../stores/auth/auth_store.dart';
import '../../stores/user/user_store.dart';
import '../widgets/last_taken_book.dart';
import '../widgets/home_screen/plan_advertisement_card.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  BookStore bookStore = serviceLocator<BookStore>();
  UserStore userStore = serviceLocator<UserStore>();



  Widget _currentReadingBook(){
    List<TakenBook>? takenBooks = serviceLocator<TakenBooksStore>().takenBooks;
    if(userStore.user!.hasSubscription==null) return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: PlanAdvertisementCard(),
    );
    if(takenBooks!=null && takenBooks.isNotEmpty) return (
        LastTakenBook(takenBook: serviceLocator<TakenBooksStore>().takenBooks[0])
    );
    return SizedBox(height: 0,);
  }
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Material(
          color: Theme.of(context).backgroundColor,
          child: ListView(children: [
            HomeHeader(),
            SizedBox(
              height: 10,
            ),
            _currentReadingBook(),
            SizedBox(
              height: 10,
            ),

            SizedBox(
              height: 10,
            ),
            // ---- Recommendation ---
            Container(
                padding: EdgeInsets.only(left: 15, right: 15), child: Column(
              children: [
                BookCarousel(
                    title: 'Рекомендации',
                    showAllUrl: '',
                    books: bookStore.topRecommendations),
                BookCarousel(
                    title: 'Новинки',
                    showAllUrl: '',
                    books: bookStore.newestBooks),
                BookCarousel(
                    title: 'Выбор недели',
                    showAllUrl: '',
                    books: bookStore.choicesOfTheWeek)
              ],
            )),
          ]));
    });
  }
}
