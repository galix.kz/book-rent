import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/book/book_store.dart';
import 'package:book_rent/ui/widgets/book_card_with_description.dart';

import '../../data/models/book/book_model.dart';
import '../widgets/book_card.dart';
import '../widgets/loading_widget.dart';

class SearchScreen extends StatefulWidget {
  @override
  State<SearchScreen> createState() => _SearchScreenState();
}


class _SearchScreenState extends State<SearchScreen> {
  late List<Book> books;
  String? searchString;

  Widget _content() {
    return Observer(builder: (_)
    {
      // if (searchString == null) {
      //   books = serviceLocator<BookStore>().topRecommendations;
      // }
      // else {

      // }

      if (searchString != null && books.isEmpty) {
        return
          SizedBox(
            child: LoadingWidget(
          isImage: false,
        ));
      }

      return SizedBox(
        child: ListView.builder(
          itemCount: books.length,
          shrinkWrap: true,
          itemBuilder: (context, index) =>
              BookCardWithDescription(book: books[index]),
        ),
      );
    });
  }

  Timer? searchOnStoppedTyping;

  void _onChangeHandler(String value) {
    const duration = Duration(milliseconds:800); // set the duration that you want call search() after that.
    if (searchOnStoppedTyping != null) {
      setState(() => searchOnStoppedTyping!.cancel()); // clear timer
    }
    setState(() => searchOnStoppedTyping = new Timer(duration, () => serviceLocator<BookStore>().searchBooks(value)));
  }

  @override
  Widget build(BuildContext context) {

    return
      Observer(builder: (_) {
        books = serviceLocator<BookStore>().searchedBooks;
        return Material(
            color: Colors.transparent,
            child:
            Container(
            padding: EdgeInsets.only(left: 15, right: 15),
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
        SizedBox(height: 40,),
        Text('Ищите книги по вкусу', style: Theme.of(context).textTheme.headline5!.apply(fontSizeDelta: 4, fontWeightDelta: 2),),
        SizedBox(height: 20,),
        TextField(
        keyboardType: TextInputType.text,
        autofocus:true,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15)
            )
        ),
        onChanged: _onChangeHandler
        ),
        Expanded(child: _content())
        ],
        )
        ,
        )
        );
      });
  }

}