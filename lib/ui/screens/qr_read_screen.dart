import 'dart:io';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/book/book_store.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:vrouter/vrouter.dart';

import '../../data/models/book/book_model.dart';

class QRReadScreen extends StatefulWidget{
  @override
  _QRReadScreen createState() => _QRReadScreen();
}

class _QRReadScreen extends State<QRReadScreen> {
  Barcode? result;
  bool qrCodeError = false;
  bool? takeBookSuccess;
  QRViewController? controller;
  Book? book;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }

    controller!.resumeCamera();
  }

  @override
  Widget build(BuildContext context) {

    return Observer(builder: (_){
      book = serviceLocator<BookStore>().bookDetail;
      return Scaffold(
        body: Column(
            children: <Widget>[
            Expanded(flex: 4, child: _buildQrView(context)),
        Expanded(
          flex: 1,
          child: FittedBox(
            fit: BoxFit.contain,
            child:
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child:  Column(
                    children: <Widget>[
                      if(qrCodeError) Text('Кажется это не наш qr', style: Theme.of(context).textTheme.bodyText2!.apply(color: Colors.red)),
                      (book != null) ?
                      ElevatedButton(onPressed: () async {
                        try{
                          await serviceLocator<BookStore>().takeBookByQr(result!.code!);
                          AwesomeDialog(
                            context: context,
                            dialogType: DialogType.SUCCES,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'Вы успешно взяли книгу',
                            desc: 'Покажите сотруднику данное сообщение',
                            dismissOnTouchOutside: false,
                            dismissOnBackKeyPress: false,
                            btnOkText: 'Показал(а)',
                            btnOkOnPress: () {
                            },
                          ).show();
                        }
                        catch(e){
                          AwesomeDialog(
                            context: context,
                            dialogType: DialogType.ERROR,
                            animType: AnimType.BOTTOMSLIDE,
                            title: 'Не удалось взять книгу',
                            desc: 'Возможно данная книга уже находится у кого-то, попробуйте позднее',
                            dismissOnTouchOutside: false,
                            dismissOnBackKeyPress: false,
                            showCloseIcon: true,
                            btnCancelText: "Закрыть",
                            btnCancelOnPress: () {
                              setState((){
                                book = null;
                              });
                            },
                          ).show();
                        }
                        await serviceLocator<BookStore>().unsetScannedQr();
                        await serviceLocator<BookStore>().unsetBookDetail();

                      }, style: ElevatedButton.styleFrom(
                          minimumSize: Size(200, 30) // put the width and height you want
                      ), child: Text('Взять книгу'),)
                          :
                      Text('Отсканируйте qr в книге', style: Theme.of(context).textTheme.bodyText1!,),
                    ],
                  ),
                ),

          )),

            ],
          ),
      );
    });

  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
        MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) async {
      setState(() async {
        result = scanData;
        try{
          book = await serviceLocator<BookStore>().getBookByQr(scanData.code!);
          qrCodeError = false;
        }
        catch(e){
          qrCodeError = true;
        }
      });
    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {

    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('no Permission')),
      );
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}