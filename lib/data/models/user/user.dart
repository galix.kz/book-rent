import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

import 'has_subscription.dart';

part 'user.g.dart';


@JsonSerializable()
class User {
  final int id;
  final String? username;
  final String? avatar;
  final String? fullname;

  @JsonKey(name: 'is_new')
  final bool? isNew;
  @JsonKey(name: 'has_subscription')
  final HasSubscription? hasSubscription;
  final String? token;

  User(
      {required this.id,
      required this.username, this.avatar, this.fullname,
      this.isNew,
      this.hasSubscription, this.token});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
