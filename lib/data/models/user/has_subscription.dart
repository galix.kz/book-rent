import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

part 'has_subscription.g.dart';

@JsonSerializable()
class HasSubscription {
  @JsonKey(name: 'left_days')
  final int leftDays;

  @JsonKey(name: 'date_till')
  final String dateTill;

  String name;

  HasSubscription({
    required this.leftDays,
    required this.name,
    required this.dateTill
  });

  factory HasSubscription.fromJson(Map<String, dynamic> json) {
    return _$HasSubscriptionFromJson(json);
  }

  Map<String, dynamic> toJson() => _$HasSubscriptionToJson(this);
}
