import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

part 'author.g.dart';

@JsonSerializable()
class Author {
  final int id;
  final String name;
  Author({
    required this.id,
    required this.name
  });

  factory Author.fromJson(Map<String, dynamic> json) {
    return _$AuthorFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AuthorToJson(this);
}
