import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';


part 'subscription_plan.g.dart';

@JsonSerializable()
class SubscriptionPlan {
  final int id;
  String name;
  int price;
  
  @JsonKey(name: 'sale_price')
  int salePrice;



  SubscriptionPlan({
    required this.id,
    required this.name,
    required this.price,
    required this.salePrice,

  });

  factory SubscriptionPlan.fromJson(Map<String, dynamic> json) {
    return _$SubscriptionPlanFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SubscriptionPlanToJson(this);
}
