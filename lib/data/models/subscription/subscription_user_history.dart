
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';


part 'subscription_user_history.g.dart';

@JsonSerializable()
class SubscriptionUserHistory {
  int user;
  int plan;
  int price;
  
  @JsonKey(name: 'date_from')
  String dateFrom;
  
  @JsonKey(name: 'date_till')
  String dateTill;




  SubscriptionUserHistory({
    required this.user,
    required this.plan,
    required this.price,
    required this.dateFrom,
    required this.dateTill,
  });

  factory SubscriptionUserHistory.fromJson(Map<String, dynamic> json) {
    return _$SubscriptionUserHistoryFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SubscriptionUserHistoryToJson(this);
}
