import 'package:book_rent/data/models/subscription/subscription_plan.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';


part 'subscription_paybox_create.g.dart';

@JsonSerializable()
class SubscriptionPayboxCreate {
  SubscriptionPlan plan;
  String? orderId;
  String? paymentId;

  SubscriptionPayboxCreate({
    required this.plan,
     this.orderId,
     this.paymentId,
  });

  factory SubscriptionPayboxCreate.fromJson(Map<String, dynamic> json) {
    return _$SubscriptionPayboxCreateFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SubscriptionPayboxCreateToJson(this);
}


