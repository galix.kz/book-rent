
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';


part 'subscription_user_history_create.g.dart';

@JsonSerializable()
class SubscriptionUserHistoryCreate {
  int user;
  int plan;
  int price;
  @JsonKey(name: 'payment_id')
  String paymentId;

  @JsonKey(name: 'order_id')
  String orderId;




  SubscriptionUserHistoryCreate({
    required this.user,
    required this.plan,
    required this.price,
    required this.orderId,
    required this.paymentId
  });

  factory SubscriptionUserHistoryCreate.fromJson(Map<String, dynamic> json) {
    return _$SubscriptionUserHistoryCreateFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SubscriptionUserHistoryCreateToJson(this);
}
