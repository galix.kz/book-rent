import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

part 'category.g.dart';

@JsonSerializable()
class BookCategory {
  final int id;
  final String name;
  BookCategory({
    required this.name,
    required this.id
  });

  factory BookCategory.fromJson(Map<String, dynamic> json) {
    return _$BookCategoryFromJson(json);
  }

  Map<String, dynamic> toJson() => _$BookCategoryToJson(this);
}
