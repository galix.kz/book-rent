import 'dart:ffi';

import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

import '../author/author.dart';
import '../shop/shop_model.dart';
import '../category/category.dart';
import './book_model.dart';

part 'book_detailed_model.g.dart';


@JsonSerializable()
class BookDetailed extends Book{

  @JsonKey(name: 'list_shops_with_books')
  List<Shop>? listShopsWithBooks;


  List<BookCategory>? categories;

  BookDetailed(
      {required int id,
        required String name,
        required String description,
        required String cover,
        required double rating, String? authors, this.listShopsWithBooks, this.categories}) : super(id: id, name: name, description: description, cover: cover, rating: rating, authors: authors);


  factory BookDetailed.fromJson(Map<String, dynamic> json) => _$BookDetailedFromJson(json);

  Map<String, dynamic> toJson() => _$BookDetailedToJson(this);
}

