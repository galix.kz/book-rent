import 'dart:ffi';

import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

import '../author/author.dart';

part 'book_model.g.dart';

@JsonSerializable()
class Book {
  final int id;
  final String name;
  final String description;
  final String cover;
  final double rating;
  final String? authors;
  Book(
      {required this.id,
      required this.name,
      required this.description,
      required this.cover,
      required this.rating, this.authors});

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);

  Map<String, dynamic> toJson() => _$BookToJson(this);
}

