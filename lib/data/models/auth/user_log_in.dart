import 'package:json_annotation/json_annotation.dart';

part 'user_log_in.g.dart';

@JsonSerializable()
class UserLogIn {
  String? phone;
  String? code;

  UserLogIn( {
    this.phone,
    this.code,
  });

  factory UserLogIn.fromJson(Map<String, dynamic> json) =>
      _$UserLogInFromJson(json);

  Map<String, dynamic> toJson() => _$UserLogInToJson(this);
}
