import 'package:json_annotation/json_annotation.dart';

part 'check_code_success.g.dart';

@JsonSerializable()
class CheckCodeSuccess {
  bool isCodeCorrect;

  CheckCodeSuccess({
      required this.isCodeCorrect,
  });

  factory CheckCodeSuccess.fromJson(Map<String, dynamic> json) =>
      _$CheckCodeSuccessFromJson(json);

  Map<String, dynamic> toJson() => _$CheckCodeSuccessToJson(this);
}
