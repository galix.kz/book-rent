import 'dart:ffi';

import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

part 'shop_model.g.dart';

@JsonSerializable()
class Shop {
  String name;
  String logo;
  String address;
  String gis2_link;
  double latitude;
  double longitude;

  Shop(
      {required this.name,
      required this.address,
      required this.logo,
      required this.gis2_link,
      required this.latitude,
      required this.longitude});

  factory Shop.fromJson(Map<String, dynamic> json) => _$ShopFromJson(json);

  Map<String, dynamic> toJson() => _$ShopToJson(this);
}
