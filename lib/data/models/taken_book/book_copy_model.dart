import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

import '../book/book_model.dart';
import '../shop/shop_model.dart';

part 'book_copy_model.g.dart';

@JsonSerializable()
class BookCopy {
  Shop? shop;

  Book book;

  

  BookCopy({
    required this.shop,
    required this.book,
  });

  factory BookCopy.fromJson(Map<String, dynamic> json) {
    return _$BookCopyFromJson(json);
  }

  Map<String, dynamic> toJson() => _$BookCopyToJson(this);
}
