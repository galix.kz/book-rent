import 'package:book_rent/data/models/taken_book/book_copy_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

part 'taken_book_model.g.dart';

@JsonSerializable()
class TakenBook {
  @JsonKey(name: 'taken_at')
  final String takenAt;

  @JsonKey(name: 'deadline_at')
  final String deadlineAt;

  @JsonKey(name: 'passed_at')
  final String? passedAt;

  @JsonKey(name: 'left_days')
  final int leftDays;
  @JsonKey(name: 'book_copy')
  final BookCopy bookCopy;

  TakenBook({
    required this.leftDays,
    required this.deadlineAt,
    this.passedAt,
    required this.takenAt,
    required this.bookCopy
  });

  factory TakenBook.fromJson(Map<String, dynamic> json) {
    return _$TakenBookFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TakenBookToJson(this);
}
