import 'package:flutter/material.dart';

import 'package:vrouter/vrouter.dart';

import '../data/models/user/user.dart';
import '../service_locator.dart';
import '../stores/auth/auth_store.dart';
import '../stores/book/book_store.dart';
import '../stores/taken_books/taken_books_store.dart';
import '../stores/user/user_store.dart';
import '../ui/screens/auth/auth_screen.dart';
import '../ui/screens/auth/confirm_code_screen.dart';
import '../ui/screens/auth/otp_screen.dart';
import '../ui/screens/auth/register_screen.dart';
import '../ui/screens/auth/welcome_screen.dart';
import '../ui/screens/book_screen.dart';
import '../ui/screens/favourite_screen.dart';
import '../ui/screens/home_screen.dart';
import '../ui/screens/initial_scren.dart';
import '../ui/screens/payment_screen.dart';
import '../ui/screens/profile_screen.dart';
import '../ui/screens/qr_read_screen.dart';
import '../ui/screens/scaffold_screen.dart';
import '../ui/screens/search_screen.dart';
import '../ui/screens/subscription_list_screen.dart';
import '../ui/screens/user_taken_books.dart';
import 'const.dart';

var routes = [
  VWidget(path: AUTH, widget: Auth1Screen(), name: AUTH, stackedRoutes: [
    VWidget(
        path: CONFIRM_CODE, widget: ConfirmPhoneScreen(), name: CONFIRM_CODE),
  ]),
  VGuard(beforeEnter: (vRedirector) async {}, stackedRoutes: [
    VWidget(
        path: SUBSCRIPTION_LIST_SCREEN_ROUTE,
        widget: SubscriptionListScreen(),
        name: SUBSCRIPTION_LIST_SCREEN_ROUTE),
  ]),
  VGuard(beforeEnter: (vRedirector) async {}, stackedRoutes: [
    VWidget(
        path: PAYMENT_SCREEN_ROUTE,
        widget: PaymentScreen(),
        name: PAYMENT_SCREEN_ROUTE),
  ]),
  VNester(
      path: '/',
      widgetBuilder: (child) =>
          ScaffoldScreen(child), // Child is the widget from nestedRoutes
      nestedRoutes: [
        VWidget(path: null, widget: InitialScreen()),
        VGuard(
            beforeEnter: (vRedirector) async {
              serviceLocator<BookStore>().getRecommendations();
              serviceLocator<BookStore>().getChoicesOfWeek();
              serviceLocator<BookStore>().getNewestBooks();
              serviceLocator<TakenBooksStore>().getTakenBooks();
            },
            stackedRoutes: [
              VWidget(path: HOME_ROUTE, widget: HomeScreen(), name: HOME_ROUTE),
            ]),
        VGuard(
            beforeEnter: (vRedirector) async {
              var id = int.parse(vRedirector.toUrl!.split('book/')[1].trim());
              serviceLocator<BookStore>().getBook(id);
            },
            stackedRoutes: [
              VWidget(
                  path: '$BOOK_ROUTE/:id',
                  widget: BookScreen(),
                  name: BOOK_ROUTE),
            ]),
        VGuard(
            beforeEnter: (vRedirector) async {
              serviceLocator<BookStore>().getLiked();
            },
            stackedRoutes: [
              VWidget(
                  path: FAVOURITE_ROUTE,
                  widget: FavouriteScreen(),
                  name: FAVOURITE_ROUTE),
            ]),
        VWidget(path: SEARCH_ROUTE, widget: SearchScreen(), name: SEARCH_ROUTE),
        VGuard(
            beforeEnter: (vRedirector) async {
              serviceLocator<TakenBooksStore>().getTakenBooks();
            },
            stackedRoutes: [
              VWidget(
                  path: PROFILE_ROUTE,
                  widget: ProfileScreen(),
                  name: PROFILE_ROUTE)
            ]),
        VGuard(
            beforeEnter: (vRedirector) async {
              serviceLocator<TakenBooksStore>().getTakenBooks();
            },
            stackedRoutes: [
              VWidget(
                  path: TAKEN_BOOKS_ROUTE,
                  widget: UserTakenBooks(),
                  name: TAKEN_BOOKS_ROUTE),
            ]),
        VGuard(beforeEnter: (vRedirector) async {}, stackedRoutes: [
          VWidget(
              path: QR_READ_SCREEN,
              widget: QRReadScreen(),
              name: QR_READ_SCREEN),
        ]),
      ])
]; // Put your VRouteElements here
