import 'dart:io';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_displaymode/flutter_displaymode.dart';
import 'package:book_rent/cubit/theme_cubit.dart';
import 'package:book_rent/router/const.dart';
import 'package:book_rent/router/routes.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/stores/subscription/subscription_store.dart';
import 'package:book_rent/stores/user/user_store.dart';

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vrouter/vrouter.dart';

import 'config/enums.dart';
import 'data/models/user/user.dart';

/// Try using const constructors as much as possible!

Future<void> main() async {
  /// Initialize packages
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  if (Platform.isAndroid) {
    await FlutterDisplayMode.setHighRefreshRate();
  }

  final tmpDir = await getTemporaryDirectory();
  await Hive.initFlutter();
  await Hive.openBox<String>('token');
  await Hive.openBox<String>('liked');
  serviceLocatorSetup(EnvironmentType.development);
  final storage = await HydratedStorage.build(
    storageDirectory: tmpDir,
  );
  FirebaseApp firebaseApp = await Firebase.initializeApp();
  WidgetsFlutterBinding.ensureInitialized();

  AwesomeNotifications().initialize(
      'resource://drawable/res_app_icon',
      [
        // Your notification channels go here
      ]
  );

  // Create the initialization for your desired push service here

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);


  HydratedBlocOverrides.runZoned(
    // () => runApp(
    //   EasyLocalization(
    //     path: 'assets/translations',
    //     supportedLocales: const [
    //       Locale('en'),
    //       Locale('de'),
    //     ],
    //     fallbackLocale: const Locale('en'),
    //     useFallbackTranslations: true,
    //     child: MyApp(),
    //   ),
    // ),
    () => runApp(
          MyApp()
    ),
    storage: storage,
  );
}
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {

  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  print("Handling a background message: ${message.messageId}");

  // Use this method to automatically convert the push data, in case you gonna use our data standard
  AwesomeNotifications().createNotificationFromJsonData(message.data);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>{

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ThemeCubit>(
      create: (context) => ThemeCubit(),
      child: BlocBuilder<ThemeCubit, ThemeState>(
        builder: (context, state) {
          return VRouter(

            beforeEnter: (vRedirector) async{
              if(vRedirector.toUrl=='/'){
                Box tokenBox = Hive.box<String>('token');
                try {
                  await serviceLocator<UserStore>().getMe();
                  await serviceLocator<SubscriptionStore>().getPlans();
                  vRedirector.toNamed(HOME_ROUTE);
                }
                on DioError catch (e) {
                  if(e.response!.statusCode == 401 && !vRedirector.toUrl!.startsWith(AUTH)){
                    vRedirector.toNamed(AUTH);
                  }
                }
              }
            },
            mode: VRouterMode.history,
            /// Localization is not available for the title.
            title: 'Flutter Production Boilerplate',
            theme: state.themeData,
            debugShowCheckedModeBanner: false,
            routes: routes,
          );
        },
      ),
    );
  }
}
