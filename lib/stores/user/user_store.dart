import 'package:book_rent/network/user_client.dart';
import 'package:book_rent/service_locator.dart';
import 'package:book_rent/data/models/auth/check_code_success.dart';
import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';
import '../../data/models/auth/user_log_in.dart';
import '../../data/models/user/user.dart';
import '../../network/auth_client.dart';

part 'user_store.g.dart';

class UserStore = _UserStore with _$UserStore;

abstract class _UserStore with Store {
  _UserStore();

  @observable
  User? user;

  @action
  void setUser(User userData){
    user = userData;
  }
  @action
  void logout(){
    Box tokenBox = Hive.box<String>('token');
    tokenBox.delete('access');
    user = null;
  }

  @action
  Future<User?> getMe() async{
    user = await serviceLocator<UserClient>().getMe();
    return user;
  }
}
