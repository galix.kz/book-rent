
import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';

import '../../config/enums.dart';
import '../../data/models/book/book_detailed_model.dart';
import '../../data/models/book/book_model.dart';
import '../../data/models/taken_book/taken_book_model.dart';
import '../../network/book_client.dart';
import '../../service_locator.dart';
import '../taken_books/taken_books_store.dart';

part 'book_store.g.dart';

class BookStore = _BookStore with _$BookStore;

abstract class _BookStore with Store {
  _BookStore();

  @observable
  ObservableFuture<List<Book>>? _bookFuture;

  @observable
  List<Book> topRecommendations = [];

  @observable
  List<Book> newestBooks = [];

  @observable
  List<Book> choicesOfTheWeek = [];

  @observable
  List<Book> searchedBooks = [];

  @observable
  List<Book?> likedBooks = [];

  @observable
  String? scannedQr;


  @observable
  BookDetailed? bookDetail;

  @action
  Future<void> getRecommendations() async {
      _bookFuture = ObservableFuture(serviceLocator<BookClient>().getBooks("recommendations"));
      topRecommendations = await _bookFuture!;
  }

  @action
  Future<void> getChoicesOfWeek() async {
    _bookFuture = ObservableFuture(serviceLocator<BookClient>().getBooks("choices_of_week"));
    choicesOfTheWeek = await _bookFuture!;
  }

  @action
  Future<void> getNewestBooks() async {
    _bookFuture = ObservableFuture(serviceLocator<BookClient>().getBooks("newest"));
    newestBooks = await _bookFuture!;
  }

  @action
  Future<void> searchBooks(String searchString) async {

    searchedBooks = await serviceLocator<BookClient>().searchBook(searchString);
  }

  // used for placehold already got info to page
  @action
  Future<void> redirectToBook(Book bookInfo) async{
    bookDetail = BookDetailed(id: bookInfo.id, name: bookInfo.name, description: bookInfo.description, cover: bookInfo.cover, rating: bookInfo.rating, authors: bookInfo.authors);
    print(bookInfo);
  }

  @action
  Future<void> getBook(int bookId) async {
    bookDetail = await serviceLocator<BookClient>().getBook(bookId);
  }


  @action
  Future<BookDetailed> getBookByQr(String qrCode) async {
    scannedQr = qrCode;
    return bookDetail = await serviceLocator<BookClient>().getBookByQR(qrCode);
  }

  @action
  Future<TakenBook> takeBookByQr(String qrCode) async {
    scannedQr = qrCode;
    TakenBook takenBook = await serviceLocator<BookClient>().takeBookByQR(qrCode);
    serviceLocator<TakenBooksStore>().addTakenBook(takenBook);
    return takenBook;
  }

  @action
  Future<void> unsetScannedQr() async {
    scannedQr = null;
  }

  @action
  Future<void> unsetBookDetail() async {
    bookDetail = null;
  }

  @action
  bool isLikedContainsBook(Book book){
    var copiedbooks = likedBooks;
    if(copiedbooks.isNotEmpty){
      var contain = copiedbooks.where((element) => element!.id == book!.id);
      return contain.isNotEmpty;
    }
   return false;
  }

  @computed
  bool get isLikedContainsDetailedBook{
    var copiedbooks = likedBooks;
    if(copiedbooks.isNotEmpty) {
      var contain = copiedbooks.where((element) =>
      element!.id == bookDetail!.id);
      return contain.isNotEmpty;
    }
    return false;
  }

  @action
  Future<void> setLiked(Book book) async {
    Box likedBox = Hive.box<String>('liked');
    print('________BOOK________');
    print(!isLikedContainsBook(book));
    if(!isLikedContainsDetailedBook){
      likedBooks.add(book);
      likedBooks = likedBooks;
      likedBox.put("liked", jsonEncode(likedBooks));
    }
    else{
      likedBooks.removeWhere((item) => item!.id == book.id);
      likedBooks = likedBooks;
      likedBox.put("liked", jsonEncode(likedBooks));
    }
  }

  @action
  Future<void> getLiked() async {
    Box likedBox = Hive.box<String>('liked');
    String cachedBooks = likedBox.get("liked", defaultValue: "[]") as String;
    print(cachedBooks);
    likedBooks = (json.decode(cachedBooks) as List).map((i) =>
        Book.fromJson(i)).toList();
  }
}
