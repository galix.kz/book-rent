
import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';
import '../../data/models/auth/user_log_in.dart';
import '../../data/models/subscription/subscription_paybox_create.dart';
import '../../data/models/subscription/subscription_plan.dart';
import '../../data/models/subscription/subscription_user_history.dart';
import '../../data/models/subscription/subscription_user_history_create.dart';
import '../../data/models/user/user.dart';
import '../../network/auth_client.dart';
import '../../network/subscription_client.dart';
import '../../network/subscription_user_history_client.dart';
import '../../service_locator.dart';

part 'subscription_store.g.dart';

class SubscriptionStore = _SubscriptionStore with _$SubscriptionStore;

abstract class _SubscriptionStore with Store {
  _SubscriptionStore();

  @observable
  List<SubscriptionPlan> plans = [];

  @observable
  SubscriptionPayboxCreate? payboxCreateData;

  @observable
  List<SubscriptionUserHistory> subscriptionUserHistory = [];


  @action
  Future<void> getPlans() async{
    plans = await serviceLocator<SubscriptionClient>().getPlans();
  }
  @action
  Future<void> createSubscriptionUserHistory(SubscriptionUserHistoryCreate createData) async{
    return subscriptionUserHistory.add(await serviceLocator<SubscriptionUserHistoryClient>().createHistory(createData));
  }

  @action
  Future<void> setPayboxCreateData(SubscriptionPayboxCreate data) async{
    payboxCreateData = data;
  }
}
