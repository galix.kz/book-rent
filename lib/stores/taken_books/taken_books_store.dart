
import 'dart:convert';
import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';

import '../../config/enums.dart';
import '../../data/models/book/book_detailed_model.dart';
import '../../data/models/book/book_model.dart';
import '../../data/models/taken_book/taken_book_model.dart';
import '../../network/book_client.dart';
import '../../network/taken_book_client.dart';
import '../../service_locator.dart';

part 'taken_books_store.g.dart';

class TakenBooksStore = _TakenBooksStore with _$TakenBooksStore;

abstract class _TakenBooksStore with Store {
  _TakenBooksStore();

  @observable
  List<TakenBook> takenBooks = [];

  @action
  Future<void> getTakenBooks() async{
    takenBooks = await serviceLocator<TakenBooksClient>().getBooks();
  }

  @action
  Future<void> addTakenBook(TakenBook book) async{
    takenBooks.add(book);
  }
}
