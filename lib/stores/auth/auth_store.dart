
import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';
import '../../data/models/auth/check_code_success.dart';
import '../../data/models/auth/user_log_in.dart';
import '../../data/models/user/user.dart';
import '../../network/auth_client.dart';
import '../../service_locator.dart';
import '../user/user_store.dart';

part 'auth_store.g.dart';

class AuthStore = _AuthStore with _$AuthStore;

abstract class _AuthStore with Store {
  _AuthStore();

  Box tokenBox = Hive.box<String>('token');

  @observable
  UserLogIn? userLogIn;

  @observable
  bool? codeStatus;

  Future<void> authByPhone(String phone) async {
    tokenBox.delete("access");
    userLogIn =
        await serviceLocator<AuthClient>().sendCode(UserLogIn(phone: phone));
  }

  Future<bool> checkVerifyCode(String code) async {
    CheckCodeSuccess response = await serviceLocator<AuthClient>().checkCode(
        UserLogIn(phone: '+7' + userLogIn!.phone.toString(), code: code));
    return response.isCodeCorrect;
  }

  Future<void> logIn() async {
    User responseUser = await serviceLocator<AuthClient>().login(UserLogIn(
        phone: '+7' + userLogIn!.phone.toString(),
        code: userLogIn!.code.toString()));
    serviceLocator<UserStore>().setUser(responseUser);
    tokenBox.put('access', responseUser.token);
  }
}
