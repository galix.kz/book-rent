const String apiURL = 'https://dry-ravine-66412.herokuapp.com/api/v1';
// const String apiURL = 'http://10.48.97.117:8000/api/v1';
const String authApiURL = '$apiURL/auth';
const String bookApiURL = '$apiURL/books';
const String userBookHistoryApiURL = '$apiURL/user-book-history';
const String subscriptionPlansApiURL = '$apiURL/subscription/plans';
const String subscriptionUserHistoryApiURL = '$apiURL/subscription/user-history';
const String userApiURL = '$apiURL/users';
