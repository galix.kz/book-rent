
enum EnvironmentType {
  development,
  production
}

enum UsernameType {
  phone,
  email,
  google,
}
enum StoreState {
  loading,
  loaded,
  empty
}