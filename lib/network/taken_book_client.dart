import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import '../config/api_routes.dart';
import '../data/models/book/book_model.dart';
import '../data/models/taken_book/taken_book_model.dart';
import '../data/models/user/user.dart';

part 'taken_book_client.g.dart';

@RestApi(baseUrl: userBookHistoryApiURL)
abstract class TakenBooksClient {
  factory TakenBooksClient(Dio dio, {String? baseUrl}) = _TakenBooksClient;

  @GET('/')
  Future<List<TakenBook>> getBooks();




}