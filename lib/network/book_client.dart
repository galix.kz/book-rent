import 'package:dio/dio.dart';

import 'package:retrofit/http.dart';

import '../config/api_routes.dart';
import '../data/models/book/book_detailed_model.dart';
import '../data/models/book/book_model.dart';
import '../data/models/taken_book/taken_book_model.dart';
import '../data/models/user/user.dart';

part 'book_client.g.dart';


@RestApi(baseUrl: bookApiURL)
abstract class BookClient {
  factory BookClient(Dio dio, {String? baseUrl}) = _BookClient;

  @GET('/')
  Future<List<Book>> getBooks(@Query("type") String type);

  @GET('/')
  Future<List<Book>> searchBook(@Query("search") String searchString);


  @POST('/get_by_qr/')
  Future<BookDetailed> getBookByQR(@Field() String qr_code);

  @GET('/{id}')
  Future<BookDetailed> getBook(@Path() int id);

  @POST('/take_by_qr/')
  Future<TakenBook> takeBookByQR(@Field() String qr_code);

}