import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import '../config/api_routes.dart';
import '../data/models/subscription/subscription_user_history.dart';
import '../data/models/subscription/subscription_user_history_create.dart';


part 'subscription_user_history_client.g.dart';

@RestApi(baseUrl: subscriptionPlansApiURL)
abstract class SubscriptionUserHistoryClient {
  factory SubscriptionUserHistoryClient(Dio dio, {String? baseUrl}) = _SubscriptionUserHistoryClient;

  @POST('/')
  Future<SubscriptionUserHistory> createHistory(@Body() SubscriptionUserHistoryCreate createData);

}