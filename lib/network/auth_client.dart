import 'package:dio/dio.dart';

import 'package:retrofit/http.dart';

import '../config/api_routes.dart';
import '../data/models/auth/check_code_success.dart';
import '../data/models/auth/user_log_in.dart';
import '../data/models/user/user.dart';
part 'auth_client.g.dart';

@RestApi(baseUrl: authApiURL)
abstract class AuthClient {
  factory AuthClient(Dio dio, {String? baseUrl}) = _AuthClient;

  @POST('/send_code/')
  Future<UserLogIn?> sendCode(@Body() UserLogIn? userLogIn);

  @POST('/check_code/')
  Future<CheckCodeSuccess> checkCode(@Body() UserLogIn? userLogIn);

  @POST('/login_or_register/')
  Future<User> login(@Body() UserLogIn? userLogIn);

}