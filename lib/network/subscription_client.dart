import 'package:dio/dio.dart';

import 'package:retrofit/http.dart';

import '../config/api_routes.dart';
import '../data/models/book/book_model.dart';
import '../data/models/subscription/subscription_plan.dart';
import '../data/models/taken_book/taken_book_model.dart';
import '../data/models/user/user.dart';

part 'subscription_client.g.dart';

@RestApi(baseUrl: subscriptionPlansApiURL)
abstract class SubscriptionClient {
  factory SubscriptionClient(Dio dio, {String? baseUrl}) = _SubscriptionClient;

  @GET('/')
  Future<List<SubscriptionPlan>> getPlans();

}