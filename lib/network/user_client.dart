import 'package:dio/dio.dart';

import 'package:retrofit/http.dart';

import '../config/api_routes.dart';
import '../data/models/auth/user_log_in.dart';
import '../data/models/user/user.dart';
part 'user_client.g.dart';

@RestApi(baseUrl: userApiURL)
abstract class UserClient {
  factory UserClient(Dio dio, {String? baseUrl}) = _UserClient;

  @GET('/me')
  Future<User?> getMe();

  @GET('/me')
  Future<List<User>> getUsers();

}