import 'package:dio/dio.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../utils/logger.dart';


InterceptorsWrapper intercepter(Dio dio) {
  Box tokenBox = Hive.box<String>('token');
  int retries = 0;

  RequestOptions getOptions(RequestOptions options) {
    print(tokenBox.keys);
    RequestOptions _options = options;
    _options.headers['USERNAME'] = 'admin';

    if (tokenBox.get('access') != null) {
      _options.headers['Authorization'] = 'Bearer ${tokenBox.get('access')}';
    }
    if (_options.headers['Content-Type'] != null) {
      _options.headers['Content-Type'] = 'application/json';
    }
    if (_options.headers['Accept'] != null) {
      _options.headers['Accept'] = 'application/json';
    }

    return _options;
  }

  return InterceptorsWrapper(
      onRequest: (options, handler) {
        return handler.next(getOptions(options));
      },
      onResponse: (response, handler) {
        retries = 0;
        return handler.next(response);
      },
      onError: (error, handler) async {
        // final AuthStore authStore = serviceLocator<AuthStore>();
        logger.i(error.response);
        return handler.next(error);
      }
  );
}

extension _AsOptions on RequestOptions {
  Options asOptions() {
    return Options(
      method: method,
      sendTimeout: sendTimeout,
      receiveTimeout: receiveTimeout,
      extra: extra,
      headers: headers,
      responseType: responseType,
      contentType: contentType,
      validateStatus: validateStatus,
      receiveDataWhenStatusError: receiveDataWhenStatusError,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
      requestEncoder: requestEncoder,
      responseDecoder: responseDecoder,
      listFormat: listFormat,
    );
  }
}
