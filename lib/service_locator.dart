import 'package:dio/dio.dart';
import 'package:book_rent/config/api_routes.dart';
import 'package:book_rent/network/book_client.dart';
import 'package:book_rent/network/subscription_client.dart';
import 'package:book_rent/network/taken_book_client.dart';
import 'package:book_rent/services/notifier_service.dart';
import 'package:book_rent/stores/book/book_store.dart';
import 'package:book_rent/stores/subscription/subscription_store.dart';
import 'package:book_rent/stores/taken_books/taken_books_store.dart';
import 'package:get_it/get_it.dart';

import 'network/auth_client.dart';
import 'network/interceptors/interceptor.dart';
import 'network/interceptors/log_interceptor.dart';
import 'network/subscription_user_history_client.dart';
import 'network/user_client.dart';
import 'stores/auth/auth_store.dart';
import 'stores/user/user_store.dart';
import 'config/enums.dart';

GetIt serviceLocator = GetIt.instance;

void serviceLocatorSetup(EnvironmentType envType) {
  serviceLocator
    ..registerFactory<UserClient>(() {
      final dio = Dio();
      dio.interceptors
        ..add(intercepter(dio))
        ..add(LoggerInterceptor(requestBody: true, responseBody: true, requestHeader: true));

      return UserClient(dio, baseUrl: userApiURL);
    })
    ..registerFactory<AuthClient>(() {
      final dio = Dio();
      dio.interceptors
        ..add(intercepter(dio))
        ..add(LoggerInterceptor(requestBody: true, responseBody: true, requestHeader: true));

      return AuthClient(dio, baseUrl: authApiURL);
    })
    ..registerFactory<BookClient>(() {
      final dio = Dio();
      dio.interceptors
        ..add(intercepter(dio))
        ..add(LoggerInterceptor(requestBody: true, responseBody: true, requestHeader: true));

      return BookClient(dio, baseUrl: bookApiURL);
    })
    ..registerFactory<TakenBooksClient>(() {
      final dio = Dio();
      dio.interceptors
        ..add(intercepter(dio))
        ..add(LoggerInterceptor(requestBody: true, responseBody: true, requestHeader: true));

      return TakenBooksClient(dio, baseUrl: userBookHistoryApiURL);
    })
    ..registerFactory<SubscriptionClient>(() {
      final dio = Dio();
      dio.interceptors
        ..add(intercepter(dio))
        ..add(LoggerInterceptor(requestBody: true, responseBody: true, requestHeader: true));

      return SubscriptionClient(dio, baseUrl: subscriptionPlansApiURL);
    })
    ..registerFactory<SubscriptionUserHistoryClient>(() {
      final dio = Dio();
      dio.interceptors
        ..add(intercepter(dio))
        ..add(LoggerInterceptor(requestBody: true, responseBody: true, requestHeader: true));

      return SubscriptionUserHistoryClient(dio, baseUrl: subscriptionUserHistoryApiURL);
    })


    ..registerSingleton<AuthStore>(AuthStore())
    ..registerSingleton<NotifierService>(NotifierService())
    ..registerSingleton<UserStore>(UserStore())
    ..registerSingleton<BookStore>(BookStore())
    ..registerSingleton<TakenBooksStore>(TakenBooksStore())
    ..registerSingleton<SubscriptionStore>(SubscriptionStore());
}
